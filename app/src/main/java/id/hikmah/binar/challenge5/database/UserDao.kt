package id.hikmah.binar.challenge5.database

import androidx.room.*

@Dao
interface UserDao {

    @Query("SELECT * FROM UserEntity WHERE username = :username")
    fun checkRegisteredUsername(username: String): List<UserEntity>

    @Query("SELECT * FROM UserEntity WHERE email = :email")
    fun checkRegisteredEmail(email: String): List<UserEntity>

    @Query("SELECT * FROM UserEntity WHERE email = :email")
    fun getUsernameByEmail(email: String): UserEntity

    @Query("SELECT * FROM UserEntity WHERE email = :email AND password = :password")
    fun checkLogin(email: String, password: String): List<UserEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(userEntity: UserEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUserDetail(profilEntity: ProfilEntity): Long

    @Query("SELECT * FROM ProfilEntity WHERE username = :username")
    fun getAllUserDetail(username: String): List<ProfilEntity>

    @Query("SELECT * FROM ProfilEntity WHERE username = :username")
    fun getAUserDetail(username: String): ProfilEntity

    @Query("UPDATE ProfilEntity SET nama_lengkap = :nama_lengkap, tgl_lahir = :tgl_lahir, alamat = :alamat WHERE username = :username")
    fun updateUserDetail(username: String, nama_lengkap: String, tgl_lahir: String, alamat: String): Int
}