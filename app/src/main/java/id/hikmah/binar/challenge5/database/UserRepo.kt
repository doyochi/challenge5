package id.hikmah.binar.challenge5.database

import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserRepo(context: Context) {
    private val userDb = UserDatabase.getInstance(context)

    suspend fun getUsername(username: String) = withContext(Dispatchers.IO) {
        userDb?.userDao()?.checkRegisteredUsername(username)
    }

    suspend fun getEmail(email: String) = withContext(Dispatchers.IO) {
        userDb?.userDao()?.checkRegisteredEmail(email)
    }

    suspend fun isLogin(email: String, password: String) = withContext(Dispatchers.IO) {
        userDb?.userDao()?.checkLogin(email, password)
    }

    suspend fun getUser(email: String) = withContext(Dispatchers.IO) {
        userDb?.userDao()?.getUsernameByEmail(email)
    }

    suspend fun insertUser(userEntity: UserEntity) = withContext(Dispatchers.IO) {
        userDb?.userDao()?.insertUser(userEntity)
    }

    suspend fun getUserDetail(username: String) = withContext(Dispatchers.IO) {
        userDb?.userDao()?.getAllUserDetail(username)
    }

    suspend fun getAUser(username: String) = withContext(Dispatchers.IO) {
        userDb?.userDao()?.getAUserDetail(username)
    }

    suspend fun insertUserDetail(profilEntity: ProfilEntity) = withContext(Dispatchers.IO) {
        userDb?.userDao()?.insertUserDetail(profilEntity)
    }

    suspend fun updateUserProfile(username: String, nama_lengkap: String, tgl_lahir: String, alamat: String) = withContext(Dispatchers.IO) {
        userDb?.userDao()?.updateUserDetail(username, nama_lengkap, tgl_lahir, alamat)
    }

}