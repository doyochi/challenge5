package id.hikmah.binar.challenge5.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import id.hikmah.binar.challenge5.R
import id.hikmah.binar.challenge5.database.UserRepo
import id.hikmah.binar.challenge5.databinding.FragmentRegisterBinding
import id.hikmah.binar.challenge5.viewModelsFactory
import id.hikmah.binar.challenge5.viewmodel.AuthViewModel

class RegisterFragment : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private val userRepo: UserRepo by lazy { UserRepo(requireContext()) }
    private val sharedPrefs by lazy { context?.getSharedPreferences("SHARED_PREFS", Context.MODE_PRIVATE) }
    private val viewModel: AuthViewModel by viewModelsFactory { AuthViewModel(userRepo, sharedPrefs) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        doRegister()
        observeData()
    }

    private fun doRegister() {
        binding.btnDaftar.setOnClickListener {
            val etUsername = binding.editUsername.text.toString()
            val etEmail = binding.editEmail.text.toString()
            val etPassword1 = binding.editPassword1.text.toString()
            val etPassword2 = binding.editPassword2.text.toString()

            if (registerValidation(etUsername, etEmail, etPassword1, etPassword2)) {
                viewModel.addUser(etUsername, etEmail, etPassword1)
            }
        }
    }

    private fun registerValidation(username: String, email: String, pass1: String, pass2: String): Boolean {
        if (username.isEmpty()) {
            binding.editUsername.error = "Mohon masukkan username"
            return false
        }

        if (email.isEmpty()) {
            binding.editEmail.error = "Mohon masukkan email"
            return false
        } else {
            binding.editEmail
        }

        if (pass1.isEmpty()) {
            binding.editPassword1.error = "Mohon masukkan password"
            return false
        }

        if (pass2.isEmpty()) {
            binding.editPassword2.error = "Mohon masukkan password"
            return false
        } else if (pass2 != pass1) {
            binding.editPassword2.error = "Password yang dimasukkan tidak sama"
            return false
        } else {
            binding.editPassword2
        }

        return true
    }

    private fun observeData() {
        viewModel.statusUsername.observe(viewLifecycleOwner) {
            if (it == false) {
                binding.editUsername.error = "Username telah terdaftar"
            }
        }

        viewModel.statusEmail.observe(viewLifecycleOwner) {
            if (it == false) {
                binding.editEmail.error = "Email telah terdaftar"
            }
        }

        viewModel.statusRegistration.observe(viewLifecycleOwner) {
            if (it == false) {
                Toast.makeText(requireContext(), "Gagal", Toast.LENGTH_SHORT).show()
            } else {
                findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
                Toast.makeText(requireContext(), "Berhasil", Toast.LENGTH_SHORT).show()
            }
        }
    }

}