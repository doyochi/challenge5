package id.hikmah.binar.challenge5.viewmodel

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.hikmah.binar.challenge5.database.UserEntity
import id.hikmah.binar.challenge5.database.UserRepo
import kotlinx.coroutines.launch

class AuthViewModel(private val userRepo: UserRepo, private val sharedPrefs: SharedPreferences?): ViewModel() {

    val statusUsername = MutableLiveData<Boolean>()
    val statusEmail = MutableLiveData<Boolean>()
    val statusRegistration = MutableLiveData<Boolean>()
    val statusLogin = MutableLiveData<String>()

    fun addUser(username: String, email: String, password: String) {
        var result1 = false
        var result2 = false
        viewModelScope.launch {
            val user = UserEntity(null, username, email, password)

            val checkUsername = userRepo.getUsername(username)
            val checkEmail = userRepo.getEmail(email)

            if (!checkUsername.isNullOrEmpty()) {
                statusUsername.value = false
            } else {
                result1 = true
            }

            if (!checkEmail.isNullOrEmpty()) {
                statusEmail.value = false
            } else {
                result2 = true
            }

            if (result1 && result2) {
                statusRegistration.value = true
                userRepo.insertUser(user)
            } else {
                statusRegistration.value = false
            }
        }
    }

    fun loginUser(email: String, password: String) {
        viewModelScope.launch {
            val checkUser = userRepo.isLogin(email, password)
            val getUsername = userRepo.getUser(email)

            if (!checkUser.isNullOrEmpty()) {
                val username = getUsername?.username
                val editor = sharedPrefs?.edit()
                editor?.apply {
                    putString("USERNAME", username)
                    putBoolean("LOGIN_STATE", true)
                    apply()
                }
                statusLogin.value = "Berhasil Login"
            } else {
                statusLogin.value = "Email atau Password salah!"
            }
        }
    }

}