package id.hikmah.binar.challenge5.viewmodel

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.hikmah.binar.challenge5.database.UserEntity
import id.hikmah.binar.challenge5.database.UserRepo
import kotlinx.coroutines.launch

class RegisterViewModel (private val userRepo: UserRepo, private val sharedPrefs: SharedPreferences?): ViewModel() {

    val isRegist = MutableLiveData<Boolean>()
    val emailIsRegist = MutableLiveData<Boolean>()
    val userIsRegist = MutableLiveData<Boolean>()

    fun addUserToDb(username: String, email: String, password: String) {
        var result1 = false
        var result2 = false

        viewModelScope.launch {
            val user = UserEntity(null, username, email, password)

            val checkUsername = userRepo.getUsername(username)
            val checkEmail = userRepo.getEmail(email)

            if (!checkUsername.isNullOrEmpty()) {
                userIsRegist.value = true
            } else {
                result1 = true
            }

            if (!checkEmail.isNullOrEmpty()) {
                emailIsRegist.value = true
            } else {
                result2 = true
            }

            if (result1 && result2) {
                isRegist.value = true
                // Jalankan query insert to db
                userRepo.insertUser(user)
            } else {
                isRegist.value = false
            }
        }
    }
}